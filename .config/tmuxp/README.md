# tmuxp sessions

These are configurations for the [tmuxp session manager for tmux](https://tmuxp.git-pull.com/).

Install tmuxp like so:

```shell
pip install --user tmuxp
```

Or, for a server:

```shell
sudo pip install tmuxp
```

Load a session, for example:

```shell
tmuxp load ~/etc/tmuxp/gitlab-3k.yaml
```

If you're in a server and already started a tmux session, you can press <kbd>A</kbd> to Append the windows defined in the configuration to the existing session. This is handy if you load `tmux` first, to get my dotfiles' custom tmux configurations:

```text
$ tmux
$ tmuxp load ~/etc/tmuxp/gitlab-single.yaml
[Loading] /home/mjl/etc/tmuxp/gitlab-single.yaml
Already inside TMUX, switch to session? yes/no
Or (a)ppend windows in the current active session?
[y/n/a] - (y, n, a):
```
