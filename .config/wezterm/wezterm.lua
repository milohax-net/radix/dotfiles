-- Pull in the wezterm API
local wezterm = require 'wezterm'

-- This will hold the configuration.
local wt = wezterm.config_builder()

-- This is where you actually apply your config choices

-- For example, changing the color scheme:
-- https://wezfurlong.org/wezterm/config/lua/wezterm.gui/get_appearance.html
-- wezterm.gui is not available to the mux server, so take care to
-- do something reasonable when this config is evaluated by the mux

-- TODO: READ https://alexplescan.com/posts/2024/08/10/wezterm/
--
--   - it does this better, and also shows how to modularize wezterm config

function get_appearance()
  if wt.gui then
    return wt.gui.get_appearance()
  end
  return 'Dark'
end

function scheme_for_appearance(appearance)
  if appearance:find 'Dark' then
    return 'Argonaut (Gogh)'
  else
    return 'Lunaria Light (Gogh)'
  end
end

--wt.color_scheme = scheme_for_appearance(get_appearance())
wt.color_scheme = 'Argonaut (Gogh)'

wt.max_fps = 120  -- ??
wt.hide_tab_bar_if_only_one_tab = true
wt.enable_scroll_bar = true
wt.default_cursor_style = 'BlinkingBlock'
wt.force_reverse_video_cursor = true
wt.cursor_blink_rate = 666
wt.window_background_gradient = {
  colors = {
    '#0a0612FF',
    '#1e1a22EE',
    '#24243eDD',
  },
  -- orientation = { Linear = { angle = 100.0 } },
  orientation = { Radial = { cx = 0.8, cy = 0.8, radius = 1.25}}
}

-- This is more trouble than it saves, particularly for automated reboots
wt.window_close_confirmation = 'NeverPrompt'
wt.skip_close_confirmation_for_processes_named = {
  'bash',
  'sh',
  'zsh',
  'fish',
  'pwsh',
  'tmux',
  'nu',
  'mc',
  'cmd.exe',
  'pwsh.exe',
  'powershell.exe',
}

wt.keys = {
  {
    key = ']',
    mods = 'CMD|ALT',
    action = wezterm.action.ToggleAlwaysOnTop,
  },
}
-- and finally, return the configuration to wezterm
return wt
