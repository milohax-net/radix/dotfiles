---------------------------------------*- mode:sql; sql-product: postgres; -*-
--
--  File:       psqlrc
--  Created:    2019-07-12
--  Language:   PostgreSQL
--  Time-stamp: <2024-11-23 20:36 mjl>
--  Platform:   PostgreSQL 9.3+
--  OS:         N/A
--  Author:     [MJL] Michael J. Lockhart <sinewalker@gmail.com>
--
--  Rights:     Copyright © 2019, 2024 Michael J. Lockhart
--              Creative Commons Attribution-ShareAlike 4.0 International
--              https://creativecommons.org/licenses/by-sa/4.0/
--
--  PURPOSE:    Sample PostgreSQL Run Commands
--
------------------------------------------------------------------------------
--
--- Commentary:
--
-- This file is loaded by psql at startup. It should live in ~/.psqlrc
-- Alternately you may set an environment variable $PSQLRC to reference where
-- you have installed it
--
-- It is based upon my personal .psqlrc, for Squiz's C7 environment
------------------------------------------------------------------------------
--
--- Bugs:
--
--    Loads command files from hard-coded location POSTGRESS_LIB
--
--    Overrides -F delimiter and other display command-line options. For
--    formatted output in files, use \copy or COPY instead.
--
------------------------------------------------------------------------------

\set QUIET on

-- Display settings (opinionated)
\pset border 2
\pset linestyle unicode
\pset format aligned
\pset pager always
\pset null '⧽null⧼'
\x auto
\setenv PSQL_EDITOR_LINENUMBER_ARG +
\setenv PAGER less
\setenv LESS '-iMSx4e -FXR'

------------------------------------------------------------------------------
-- COMMANDS

---- LOAD psql command library FROM WELL KNOWN LOCATION
\set POSTGRESS_LIB /opt/psqlrc

\set boot '\\i :POSTGRESS_LIB/boot.sql'

:boot

------------------------------------------------------------------------------

\timing on

-- psql special variables

\set HISTCONTROL ignoreboth
\set HISTSIZE 32767
\set HISTFILE ~/.psql_history- :DBNAME-:USER
\set COMP_KEYWORD_CASE upper
\set ON_ERROR_ROLLBACK on

\set idle_in_transaction_session_timeout = 0;
\set statement_timeout = 0;

\set SIMPLE1 '%/%R%x%# '
\set COLOUR1 '%[%033[1;37;42m%]%x%[%033[m%]%[%033[1;32m%]%n%[%033[1;34m%]@%[%033[1;31m%]%/%[%033[1;37;42m%]%x%[%033[m%]%[%033[1;36m%]%R%[%033[1;92m%]%#%[%033[0;37m%] '

\set SIMPLE2 '%x%/%x%R%# '
\set COLOUR2 '%[%033[1;37;42m%]%x%[%033[1;93;40m%]%/%[%033[1;37;42m%]%x%[%033[m%]%[%033[1;95m%]%R%[%033[1;92m%]%#%[%033[0;37m%] '

\set simple_prompt '\\set PROMPT1 :SIMPLE1 \\set PROMPT2 :SIMPLE2'
\set colour_prompt '\\set PROMPT1 :COLOUR1 \\set PROMPT2 :COLOUR2'
:colour_prompt

:show_database_sizes

\set QUIET off
