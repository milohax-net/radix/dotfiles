---------------------------------------*- mode:sql; sql-product: postgres; -*-
--
--  Language:   PostgreSQL
--  Platform:   PostgreSQL 9.3+
--  OS:         N/A
--  Author:     [MJL] Michael J. Lockhart <sinewalker@gmail.com>
--
--  Rights:     Copyright © 2019, 2024 Michael J. Lockhart
--              Creative Commons Attribution-ShareAlike 4.0 International
--              https://creativecommons.org/licenses/by-sa/4.0/
--
--  PURPOSE:    Generic PostgreSQL DBA Commands
--
------------------------------------------------------------------------------
--
--- Commentary:
--
-- Some useful SQL shortcuts for doing general PostgreSQL DBA work
--
------------------------------------------------------------------------------
--
--- Bugs:
--
------------------------------------------------------------------------------

\echo '\t--> dba.sql'

PREPARE _ps_cache_hit_rates AS
  SELECT
    SUM(heap_blks_read) AS heap_read,
    SUM(heap_blks_hit)  AS heap_hit,
    (SUM(heap_blks_hit) - SUM(heap_blks_read)) / SUM(heap_blks_hit) AS ratio
  FROM
    pg_statio_user_tables;
\set show_cache_hit_rates 'EXECUTE _ps_cache_hit_rates;'

PREPARE _ps_index_usage_rates AS
  SELECT
    relname,
    100 * idx_scan / (seq_scan + idx_scan) percent_of_times_index_used,
    n_live_tup
    rows_in_table
  FROM
    pg_stat_user_tables
  ORDER BY n_live_tup DESC;
\set show_index_usage_rates 'EXECUTE _ps_index_usage_rates;'

PREPARE _ps_indecies_in_cache AS
  SELECT
    SUM(idx_blks_read) AS idx_read,
    SUM(idx_blks_hit)  AS idx_hit,
    (SUM(idx_blks_hit) - SUM(idx_blks_read)) / SUM(idx_blks_hit) AS ratio
  FROM
    pg_statio_user_indexes;
\set show_indecies_in_cache 'EXECUTE _ps_indecies_in_cache;'

PREPARE _ps_database_sizes AS
  SELECT
    datname AS database,
    pg_size_pretty(pg_database_size(datname)) AS size
  FROM
    pg_database
  ORDER BY pg_database_size(datname) DESC;
\set show_database_sizes 'EXECUTE _ps_database_sizes;'

PREPARE _ps_deadlocks AS
  SELECT blocked_locks.pid    AS blocked_pid,
    blocked_activity.usename  AS blocked_user,
    blocking_locks.pid        AS blocking_pid,
    blocking_activity.usename AS blocking_user,
    blocked_activity.query    AS blocked_statement,
    blocking_activity.query   AS current_statement_in_blocking_process
  FROM  pg_catalog.pg_locks         blocked_locks
    JOIN pg_catalog.pg_stat_activity blocked_activity  ON blocked_activity.pid = blocked_locks.pid
    JOIN pg_catalog.pg_locks         blocking_locks
      ON blocking_locks.locktype = blocked_locks.locktype
      AND blocking_locks.DATABASE IS NOT DISTINCT FROM blocked_locks.DATABASE
      AND blocking_locks.relation IS NOT DISTINCT FROM blocked_locks.relation
      AND blocking_locks.page IS NOT DISTINCT FROM blocked_locks.page
      AND blocking_locks.tuple IS NOT DISTINCT FROM blocked_locks.tuple
      AND blocking_locks.virtualxid IS NOT DISTINCT FROM blocked_locks.virtualxid
      AND blocking_locks.transactionid IS NOT DISTINCT FROM blocked_locks.transactionid
      AND blocking_locks.classid IS NOT DISTINCT FROM blocked_locks.classid
      AND blocking_locks.objid IS NOT DISTINCT FROM blocked_locks.objid
      AND blocking_locks.objsubid IS NOT DISTINCT FROM blocked_locks.objsubid
      AND blocking_locks.pid != blocked_locks.pid

    JOIN pg_catalog.pg_stat_activity blocking_activity ON blocking_activity.pid = blocking_locks.pid
  WHERE NOT blocked_locks.GRANTED
  ORDER BY blocking_pid ASC;
\set show_deadlocks 'EXECUTE _ps_deadlocks;'

PREPARE _ps_locks AS
  SELECT
    a.datname,
    l.relation::regclass,
    l.transactionid,
    l.mode,
    l.granted,
    a.usename,
    a.query,
    a.query_start,
    AGE(NOW(),a.query_start) AS "age",
    a.pid
  FROM
    pg_stat_activity a
    JOIN pg_locks l
      ON l.pid = a.pid
  ORDER BY
    a.query_start;
\set show_locks 'EXECUTE _ps_locks;'

\set show_master_replication_stats 'SELECT * FROM pg_stat_replication;SELECT pg_current_xlog_location();'

\set show_pgsql_data_sizes '\\! du -sh $PGDATA/*|sort -rh'

\set show_top_ten_pgsql_files '\\! du -sh $PGDATA/*|sort -rh|head -10'

PREPARE _ps_running_queries AS
  SELECT
    pid,
    age(clock_timestamp(), query_start),
    usename,
    datname,
    query
  FROM
    pg_stat_activity
  WHERE
    query != '<IDLE>' AND query NOT ILIKE '%pg_stat_activity%'
  ORDER BY query_start DESC;
\set show_running_queries 'EXECUTE _ps_running_queries;'

PREPARE _ps_slave_repl_lag AS
  SELECT
    EXTRACT(EPOCH FROM (NOW() - pg_last_xact_replay_timestamp()))::integer AS lag_seconds;
\set show_slave_replication_lag 'EXECUTE _ps_slave_repl_lag;'

\set show_slave_replication_stats 'SELECT pg_last_xlog_receive_location(); SELECT pg_last_xlog_replay_location();'

PREPARE _ps_table_sizes AS
  SELECT
    relname AS objectname,
    relkind AS objecttype,
    reltuples AS "#entries", pg_size_pretty(relpages::bigint*8*1024) AS size
  FROM
    pg_class
  ORDER BY relpages DESC;
\set show_table_sizes 'EXECUTE _ps_table_sizes;'

PREPARE _ps_table_sizes_top_ten AS
  SELECT
    relname AS objectname,
    relkind AS objecttype,
    reltuples AS "#entries",
    pg_size_pretty(relpages::bigint*8*1024) AS size
  FROM
    pg_class
  ORDER BY relpages DESC LIMIT 10;
\set show_top_ten_tables 'EXECUTE _ps_table_sizes_top_ten;'

\set do_vacuum 'VACUUM (VERBOSE, ANALYZE);'

PREPARE _ps_curr_users AS
  SELECT *
  FROM pg_stat_activity
  WHERE query NOT LIKE '<%';
\set show_database_users 'EXECUTE _ps_curr_users;'

PREPARE _ps_describe_ps(text) AS
  SELECT
    name,
    statement
  FROM pg_prepared_statements
  WHERE name ILIKE '%'||$1||'%';
\set list_statement 'EXECUTE _ps_describe_ps'

\set show_statements 'SELECT prepare_time,name,parameter_types FROM pg_prepared_statements ORDER BY name;'

PREPARE _ps_describe_function(text) AS
  SELECT
    n.nspname AS function_schema,
    p.proname AS function_name,
    l.lanname AS function_language,
    CASE
      WHEN l.lanname = 'internal' THEN p.prosrc
      ELSE pg_get_functiondef(p.oid)
      END AS definition,
    pg_get_function_arguments(p.oid) AS function_arguments,
    t.typname AS return_type
  FROM pg_proc p
    LEFT JOIN pg_namespace n ON p.pronamespace = n.oid
    LEFT JOIN pg_language l ON p.prolang = l.oid
    LEFT JOIN pg_type t ON t.oid = p.prorettype
  WHERE p.proname ILIKE '%' || $1 || '%';
\set list_function 'EXECUTE _ps_describe_function'

PREPARE _ps_list_functions AS
  SELECT
    n.nspname AS function_schema,
    p.proname AS function_name,
    l.lanname AS function_language,
    pg_get_function_arguments(p.oid) AS function_arguments,
    t.typname AS return_type
  FROM pg_proc p
    LEFT JOIN pg_namespace n ON p.pronamespace = n.oid
    LEFT JOIN pg_language l ON p.prolang = l.oid
    LEFT JOIN pg_type t ON t.oid = p.prorettype
  WHERE n.nspname LIKE 'pg_temp%'
  ORDER BY
    function_schema,
    function_name;
\set show_functions 'EXECUTE _ps_list_functions;'

-- more modern queries from
-- https://gitlab.com/gitlab-com/support/support-training/-/issues/3846#note_2145037430

PREPARE _ps_locks_info AS
  SELECT
    pid,
    locktype,
    mode,
    granted,
    relation::regclass
  FROM pg_locks;
\set show_locks_info 'EXECUTE _ps_locks_info;'

\set show_backend_pid 'SELECT pg_backend_pid();'

PREPARE _ps_long_running_sessions AS
  SELECT
    pid,
    usename,
    application_name,
    query_start,
    wait_event_type,
    wait_event,
    state,
    query
  FROM pg_stat_activity
  WHERE state <> 'idle'
    AND query_start < now() - interval '5 seconds';
\set show_long_running_sessions 'EXECUTE _ps_long_running_sessions;'

