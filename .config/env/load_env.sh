#!/usr/bin/env bash
# Load all env files using bash
# This allows the properties to contain bash logic

set -o allexport
for prop in ${CONFIG-~/.config}/env/*.env; do
  source ${prop}
done
set +o allexport