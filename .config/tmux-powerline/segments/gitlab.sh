# shellcheck shell=bash
# Print a symbol and GitLab version, if version-manifest exists on the host

if patched_font_in_use; then
	TMUX_POWERLINE_SEG_GITLAB_SYMBOL=""
else #utf-8 chars
	TMUX_POWERLINE_SEG_GITLAB_SYMBOL="GL"
fi

TMUX_POWERLINE_SEG_GITLAB_MANIFEST="/opt/gitlab/version-manifest.txt"

generate_segmentrc() {
	read -r -d '' rccontents <<EORC
#GitLab Symbol and version manifest
export TMUX_POWERLINE_SEG_GITLAB_SYMBOL="${TMUX_POWERLINE_SEG_GITLAB_SYMBOL}"
export TMUX_POWERLINE_SEG_GITLAB_MANIFEST="${TMUX_POWERLINE_SEG_GITLAB_MANIFEST}"
EORC
	echo "$rccontents"
}

run_segment() {
	[[ -e ${TMUX_POWERLINE_SEG_GITLAB_MANIFEST} ]] || return 0
	awk -v gl=${TMUX_POWERLINE_SEG_GITLAB_SYMBOL} \
		'/^gitlab-[ce]e/ {print gl, $2; exit}' \
		${TMUX_POWERLINE_SEG_GITLAB_MANIFEST} 2> /dev/null
}
