# shellcheck shell=bash
# Print ALL THE THINGS with Starship

# Source lib to get the function get_tmux_pwd
# shellcheck source=lib/tmux_adapter.sh
source "${TMUX_POWERLINE_DIR_LIB}/tmux_adapter.sh"

#TODO Find out where the starship configuration should live
#     An alternative is in ${TMUX_POWERLINE_DIR_SEGMENTS}
#     or ${TMUX_POWERLINE_DIR_USER_THEMES}
#     or {TMUX_POWERLINE_DIR_USER_SEGMENTS}
#     but these don't ship with TPM
#     Another alternative is in ${TMUX_POWERLINE_DIR_HOME}/config
TMUX_POWERLINE_SEG_STARSHIP_CONFIG=${TMUX_POWERLINE_SEG_STARSHIP_CONFIG:-${TMUX_POWERLINE_DIR_THEMES}/starship.toml}

#testing - prefer to use tmux environment over filesystem
TMUX_POWERLINE_SEG_STARSHIP_ENV=$TMP/venv.txt

generate_segmentrc() {
	read -r -d '' rccontents <<EORC
# Path to Starship configuration.
export TMUX_POWERLINE_SEG_STARSHIP_CONFIG=${TMUX_POWERLINE_SEG_STARSHIP_CONFIG}
EORC
	echo "$rccontents"
}

__get_virtual_env(){
	#TODO: inherit environment from pane for virtualenv
	#      See https://gitlab.com/milohax-net/radix/dotfiles/-/issues/18
	if VIRTUAL_ENV=$(tmux show-environment \
			| awk -F= '/VIRTUAL_ENV/ {print $2}')
	then
		export VIRTUAL_ENV
		# test -z $VIRTUAL_ENV  && unset VIRTUAL_ENV
	else
		unset VIRTUAL_ENV
	fi
}

run_segment() {
	type starship > /dev/null 2>&1 || return 1

	cd $(get_tmux_cwd)
	__get_virtual_env
	export STARSHIP_CONFIG=${TMUX_POWERLINE_SEG_STARSHIP_CONFIG}
	STARSHIP=$(starship prompt | tr -d '\n\r' | tr -s ' ')
	[[ -n ${STARSHIP} ]] && echo ${STARSHIP}
}
