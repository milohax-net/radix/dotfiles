# shellcheck shell=bash
# Print a symbol indicating synchronized-panes mode

if patched_font_in_use; then
	TMUX_POWERLINE_SEG_SYNC_SYMBOL="↺"
else #utf-8 chars
	TMUX_POWERLINE_SEG_SYNC_SYMBOL="S"
fi

generate_segmentrc() {
	read -r -d '' rccontents <<EORC
#SYNC Symbol
export TMUX_POWERLINE_SEG_SYNC_SYMBOL="${TMUX_POWERLINE_SEG_SYNC_SYMBOL}"
EORC
	echo "$rccontents"
}

run_segment() {
	tmux show-options -w|grep 'synchronize-panes on' &> /dev/null || return 0
  echo ${TMUX_POWERLINE_SEG_SYNC_SYMBOL}
}
