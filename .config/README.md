# Dotfile Hacking

Here are some notes for hacking my dotfiles.

1. Ideally, configurations should go into [$XDG_CONFIG_HOME][xdg-spec] if the utility allows this. The default value of this location is `~/.config`, so add configuration into `.config` in the git repository
1. Prefer prefixing commits by the name of the utility being reconfigured, over using branches.
    - This is because sometimes an experimental change in a utility takes a very long time, and if the dotfiles are checked out to a specific branch to facilitate this, then either you have to be continually rebasing from `master` to keep a machine up-to-date, or else you risk lots of confusing and unrelated conflicts at the end.
    - Exception for this is for contributing MRs from a fork (which might almost be never, for a single-person dotfiles project, but if someone other than myself wants to suggest a contribution, then you're welcome!)
1. Use tags sparingly, and follow [semver][semver].
1. My indentation style changed back-and-forth over the years. Now I'm working towards TWO SPACES.
    - Phylosophically I prefer TABs (and editor configured to two-character tab stops), but not all configuration files support it (e.g. YAML doesn't)
    - Two spaces works better for deeply nested codes to fit within 80 columns
1. Wrap text at colmn 72(preferred), 80(ideal max), or 100 (if you must). Don't exceed 120.
    - This leaves room for `git blame` or other annotations
    - Markdown and plain text files should _not_ have hard wraps
    - Use a tool like [Rewrap for VSCode][rewrap] or emacs [fill-paragraph][emacs-fill] or [refill-mode][emacs-refill]
1. It is not worth trying to unify naming conventions or other styles across the configuration of different utilities. Just follow the utility's conventions and keep each utility's configurations consistent to itself.
1. Some utilities are configured with environment variables as well as configuration files. Place these within the files in `.config/env` and they will be sourced by Bash and Fish.
    - The `.config/env/places.env` file is a good place to configure non-standard places for libraries, such as Ansible, ASDF, Cargo, and so on, to keep them out of `$HOME`
1. Prefer each utility's local customization method over templating tools like `yadm`, `ansible`, and so on.
    - This repository is about configuring utilities, and aims to be independent of the installation method
    - Ideally the configurations should be the same everywhere anyway. Cases where they cannot be are exceptions
    - This guideline is to keep the configurations independent of how they are installed. At worst case, a simple/manual copy or link should "just work".
    - But be pragmatic. This isn't an absolute rule.

---

[xdg-spec](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html)

[semver](https://semver.org/)

[rewrap](https://stkb.github.io/Rewrap/)

[emacs-fill](https://www.emacswiki.org/emacs/FillParagraph)

[emacs-refill](https://www.emacswiki.org/emacs/RefillMode)
