#!/usr/bin/env bash

function get_windows() {
  # Cache all windows data once
  local workspace="${1}"
  if [[ -z ${workspace} ]]; then
    aerospace list-windows --all --json
  else
    aerospace list-windows --workspace "${workspace}" --json
  fi
}

function find_console_window() {
  # Find the console window if it exists
  # Use cached windows data to find console
  local windows="${1}"

  echo "${windows}" \
    | jq --arg title "${CONSOLE_TITLE}" \
      '.[] | select(."window-title" == $title) | ."window-id"'
}

CONSOLE_TITLE="💻 Console"
# Cache the full window list once
ALL_WINDOWS=$(get_windows)
WINDOW_ID=$(find_console_window "${ALL_WINDOWS}")
echo ${WINDOW_ID}

if [[ -z ${WINDOW_ID} ]]; then

  # Console doesn't existe
  # Use the current shell's terminal as a new console window

  # Set this window's title so that it can be found
  echo -ne "\033]0;${CONSOLE_TITLE}\007"
  NEW_WINDOWS=$(get_windows)  # Need fresh data after creating window
  WINDOW_ID=$(find_console_window "${NEW_WINDOWS}")
  echo "Made → ${CONSOLE_TITLE} (${WINDOW_ID}) ←"

  # switch to floating layout and focus the new console window
  aerospace layout --window-id ${WINDOW_ID} floating
  aerospace focus --window-id ${WINDOW_ID}

  # Since the window is now in floating layout, AeroSpace can't position
  # it any more. Also, AeroSpace uses pixel dimensions, but can't tell me
  # the screen size. So, I'm relying on another app, Raycast, to do
  # this. See https://manual.raycast.com/deeplinks
  open -g raycast://extensions/raycast/window-management/top-half
  ~/etc/tmuxp/console "${CONSOLE_TITLE}"

else

  # Console exists.

  # Hide or show it by moving among focussed and 'Ω' workspaces
  echo huzah! Is it hidden?

  OMEGA=Ω
  FOCUS=$(aerospace list-workspaces --focused)

  # Reuse the same window list for both checks
  WORKSPACE_WINDOWS=$(get_windows "${OMEGA}")
  HIDDEN=$(find_console_window "${WORKSPACE_WINDOWS}")

  FOCUS_WINDOWS=$(get_windows "${FOCUS}")
  CURRENT=$(find_console_window "${FOCUS_WINDOWS}")

  if [[ -n ${HIDDEN} ]]; then
    echo yep - show
    aerospace move-node-to-workspace ${FOCUS} \
      --window-id ${HIDDEN} \
      --focus-follows-window
    # Since it may have come from a different-sized monitor
    open -g raycast://extensions/raycast/window-management/maximize-width
  elif [[ -n ${CURRENT} ]]; then
      echo nope - hide
      aerospace move-node-to-workspace ${OMEGA} \
        --window-id ${CURRENT}
  else
    echo on another workspace - bring it here
    aerospace move-node-to-workspace ${FOCUS} \
      --window-id ${WINDOW_ID} \
      --focus-follows-window
    open -g raycast://extensions/raycast/window-management/maximize-width
  fi
fi
