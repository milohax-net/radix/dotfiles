#!/usr/bin/env bash

# msg=$(aerospace \
#   list-workspaces \
#     --empty no \
#     --monitor all \
#     --format '%{workspace} > %{monitor-name}'\
#   )

# msg=$(aerospace \
#   list-windows \
#     --all \
#     --format '%{workspace} > %{monitor-name} > %{app-name} > %{window-title}'\
#   | cut -c -65|sort
#   )

#            printf "\t\t%s%s\n", "", $3, ($4 ? OFS $4 : "")
export LANG=en_AU.UTF-8

msg=$(aerospace \
  list-windows \
    --all \
    --format '%{workspace} > %{monitor-name} > (%{app-name}) > %{window-title}'\
  | cut -c -60 \
  | sort \
  | awk '
    BEGIN { FS = " > "; OFS = " > " }
    {
      if ($1 != prev1 || $2 != prev2) {
        print $1 OFS $2
        prev1 = $1
        prev2 = $2
      }
      printf "\t\t◳\t"
      for (i=3; i<=NF; i++) {
        printf "%s%s", $i, (i<NF ? OFS : "")
      }
      printf "\n"
    }
    '
)

cat << DIALOG | osascript
set msg to "$msg"

-- this needs to be a single long line for AppleScript to parse it properly

--set the dialogResult to display dialog msg buttons {"OK"} default button "OK" with title "Workspaces"
set the dialogResult to display dialog msg buttons {"OK", "Edit"} default button "OK" with title "Workspace > Display : Windows"
--set the dialogResult to display dialog msg buttons {"Reload", "Edit", "Toggle"} default button "Reload" with title "Workspaces"
set the action to the button returned of the dialogResult
-- display dialog action

-- review https://scriptingosx.com/2022/04/launching-scripts-3-from-applescripts/
if action is "Reload" then
  -- this takes a long time
  do shell script "/opt/homebrew/bin/aerospace reload-config"
else if action is "Edit" then
  do shell script "/usr/local/bin/code --new-window ~/etc/aerospace/aerospace.toml"
else if action is "Toggle" then
  -- this also takes a long time
  do shell script "/opt/homebrew/bin/aerospace enable toggle"
end if
DIALOG


