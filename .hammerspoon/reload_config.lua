-- See https://www.hammerspoon.org/go/#fancyreload 
myConfigWatcher = nil

function reloadConfig(files)
  doReload = false
  for _,file in pairs(files) do
    if file:sub(-4) == '.lua' then
      doReload = true
    end
  end
  if doReload then
    hs.notify.new({title='Hammerspoon', informativeText='configuration reloaded'}):send()
    hs.reload()
  end
end

myConfigWatcher = hs.pathwatcher.new(os.getenv('HOME') .. '/.hammerspoon/', reloadConfig):start()
