function windowFrames()
  local win = hs.window.focusedWindow()
  local frm = win:frame()
  local scr = win:screen()
  local max = scr:frame()
  return win, scr, frm, max
end

function winMaximise()
  local win, scr, frm, max = windowFrames()
  frm.x = max.x
  frm.y = max.y
  frm.w = max.w
  frm.h = max.h
  win:setFrame(frm)
end

function winAlmostMaximize()
  local win, scr, frm, max = windowFrames()
  local frameBorder = winStepSize * 1.5
  frm.x = max.x + frameBorder
  frm.y = max.y + (frameBorder * 0.75)
  frm.w = max.w - (frameBorder * 2)
  frm.h = max.h - (frameBorder * 1.75)
  win:setFrame(frm)
end

function winLeftHalf()
  local win, scr, frm, max = windowFrames()
  frm.x = max.x
  frm.y = max.y
  frm.w = max.w / 2
  frm.h = max.h
  win:setFrame(frm)
end

function winRightHalf()
  local win, scr, frm, max = windowFrames()
  frm.x = max.x + (max.w / 2)
  frm.y = max.y
  frm.w = max.w / 2
  frm.h = max.h
  win:setFrame(frm)
end

function winTopHalf()
  local win, scr, frm, max = windowFrames()
  frm.x = max.x
  frm.y = max.y
  frm.w = max.w
  frm.h = max.h / 2
  win:setFrame(frm)
end

function winBottomHalf()
  local win, scr, frm, max = windowFrames()
  frm.x = max.x
  frm.y = max.y + (max.h / 2)
  frm.w = max.w
  frm.h = max.h / 2
  win:setFrame(frm)
end

function winTopLeft()
  local win, scr, frm, max = windowFrames()
  frm.x = max.x
  frm.y = max.y
  frm.w = max.w / 2
  frm.h = max.h / 2
  win:setFrame(frm)
end

function winTopRight()
  local win, scr, frm, max = windowFrames()
  frm.x = max.x + (max.w / 2)
  frm.y = max.y
  frm.w = max.w / 2
  frm.h = max.h / 2
  win:setFrame(frm)
end

function winBottomLeft()
  local win, scr, frm, max = windowFrames()
  frm.x = max.x
  frm.y = max.y + (max.h / 2)
  frm.w = max.w / 2
  frm.h = max.h / 2
  win:setFrame(frm)
end

function winBottomRight()
  local win, scr, frm, max = windowFrames()
  frm.x = max.x + (max.w / 2)
  frm.y = max.y + (max.h / 2)
  frm.w = max.w / 2
  frm.h = max.h / 2
  win:setFrame(frm)
end

function winFirstThird()
  local win, scr, frm, max = windowFrames()
  frm.x = max.x
  frm.y = max.y
  frm.w = max.w / 3
  frm.h = max.h
  win:setFrame(frm)
end

function winCenterThird()
  local win, scr, frm, max = windowFrames()
  frm.x = max.x + (max.w / 3)
  frm.y = max.y
  frm.w = max.w / 3
  frm.h = max.h
  win:setFrame(frm)
end

function winCenterHalf()
  local win, scr, frm, max = windowFrames()
  frm.x = max.x + (max.w / 4)
  frm.y = max.y
  frm.w = max.w / 2
  frm.h = max.h
  win:setFrame(frm)
end

function winTopCenter()
  local win, scr, frm, max = windowFrames()
  frm.x = max.x + (max.w / 4)
  frm.y = max.y
  frm.w = max.w / 2
  frm.h = max.h / 2
  win:setFrame(frm)
end

function winBottomCenter()
  local win, scr, frm, max = windowFrames()
  frm.x = max.x + (max.w / 4)
  frm.y = max.y + (max.h / 2)
  frm.w = max.w / 2
  frm.h = max.h / 2
  win:setFrame(frm)
end

function winLastThird()
  local win, scr, frm, max = windowFrames()
  frm.x = max.x + ((max.w / 3) * 2)
  frm.y = max.y
  frm.w = max.w / 3
  frm.h = max.h
  win:setFrame(frm)
end

function winFirstTwoThirds()
  local win, scr, frm, max = windowFrames()
  frm.x = max.x
  frm.y = max.y
  frm.w = (max.w - (max.w / 3))
  frm.h = max.h
  win:setFrame(frm)
end

function winLastTwoThirds()
  local win, scr, frm, max = windowFrames()
  frm.x = max.x + (max.w / 3)
  frm.y = max.y
  frm.w = (max.w - (max.w / 3))
  frm.h = max.h
  win:setFrame(frm)
end

winStepSize = 32
winMinWidth = 400
winMinHeight = 300

function winShrink()
  local win, scr, frm, max = windowFrames()
  local oldx = frm.x
  local oldy = frm.y
  frm.x = frm.x + winStepSize
  frm.y = frm.y + winStepSize
  frm.w = frm.w - (winStepSize * 2)
  frm.h = frm.h - (winStepSize * 2)

  if frm.w <= winMinWidth then
    frm.w = winMinWidth
    frm.x = oldx
  end
  if frm.h <= winMinHeight then
    frm.h = winMinHeight
    frm.y = oldy
  end
  win:setFrame(frm)
end

function winGrow()
  local win, scr, frm, max = windowFrames()
  frm.x = frm.x - winStepSize
  frm.y = frm.y - winStepSize
  frm.w = frm.w + (winStepSize * 2)
  frm.h = frm.h + (winStepSize * 2)

  if frm.x <= 0 then
    frm.x = 0
  end
  if frm.y <= max.y then
    frm.y = max.y
  end
  if frm.w >= max.w then
    frm.w = max.w
  end
  if frm.h >= max.h then
    frm.h = max.h
  end
  win:setFrame(frm)
end

function winTaller()
  local win, scr, frm, max = windowFrames()
  frm.y =frm.y - winStepSize
  frm.h = frm.h + (winStepSize * 2)
  if frm.y <= max.y then
    frm.y = max.y
  end
  if frm.w >= max.w then
    frm.w = max.w
  end
  win:setFrame(frm)
end

function winShorter()
  local win, scr, frm, max = windowFrames()
  local oldy = frm.y
  frm.y = frm.y + winStepSize
  frm.h = frm.h - (winStepSize * 2)

  if frm.h <= winMinHeight then
    frm.h = winMinHeight
    frm.y = oldy
  end
  win:setFrame(frm)
end

function winWider()
  local win, scr, frm, max = windowFrames()
  frm.x = frm.x - winStepSize
  frm.w = frm.w + (winStepSize * 2)

  if frm.x <= 0 then
    frm.x = 0
  end
  if frm.w >= max.w then
    frm.w = max.w
  end
  win:setFrame(frm)
end

function winNarrower()
  local win, scr, frm, max = windowFrames()
  local oldx = frm.x
  frm.x = frm.x + winStepSize
  frm.w = frm.w - (winStepSize * 2)

  if frm.w <= winMinWidth then
    frm.w = winMinWidth
    frm.x = oldx
  end
  win:setFrame(frm)
end

function winNextScreen()
  local win = hs.window.focusedWindow()
  local screen = win:screen()
  win:moveToScreen(screen:next())
end

function winPrevScreen()
  local win = hs.window.focusedWindow()
  local screen = win:screen()
  win:moveToScreen(screen:previous())
end

function winToScreen(screen)
  return function()
    local win = hs.window.focusedWindow()

    print(screen)
    win:moveToScreen(screen)
  end
end
