kb_omega = {'cmd', 'alt', 'ctrl'}
kb_macro = {'ctrl', 'alt'}
kb_hyper = {'cmd', 'alt'}

function setKBLayout(layout)
  hs.keycodes.setLayout(layout)
  hs.alert.show('⌨️ Keyboard: ' .. layout)
  hs.timer.doAfter(5, hs.reload) -- recalculate key mappings and hotkeys
end

function toggleKbLayout()
  if hs.keycodes.currentLayout() == 'Australian' then
    layoutName = 'Dvorak'
  else
    layoutName = 'Australian'
  end
  setKBLayout(layoutName)
end

function ejectBackupDrive()
  if hs.fs.volume.eject('/Volumes/LOCKHART') then
    hs.alert.show ('⏏ Ejected LOCKHART')
  end
end

function bindKeys()

  hs.hotkey.bind(kb_omega, 'a', hs.reload)
  hs.hotkey.bind(kb_omega, 'm', toggleKbLayout)
  hs.hotkey.bind(kb_omega, '\\', ejectBackupDrive)

-- Block Hiding windows:
-- it's silly, and I hide Firefox too often when I want history
  hs.hotkey.bind('cmd','h', doNothing)

--- Launch applications
-- MJL20240530 consider using Raycast for these too
--             And I want to make key mappings in Oryx for my Planck
--  hs.hotkey.bind(kb_omega, 'f12', function() hs.application.launchOrFocus('iTerm') end)
  hs.hotkey.bind(kb_macro, 'f10', function() hs.application.launchOrFocus('Visual Studio Code') end)
  hs.hotkey.bind(kb_macro, 'f9', function() hs.application.launchOrFocus('Slack') end)
  hs.hotkey.bind(kb_macro, 'f8', function() hs.application.launchOrFocus('Spotify') end) -- or 'Clementine'
  hs.hotkey.bind(kb_macro, 'f7', function() hs.application.launchOrFocus('Zoom.us') end)
  hs.hotkey.bind(kb_macro, 'f6', function() hs.application.launchOrFocus('Firefox') end)

-- Window management
-- MJL20240530 blocking these out: do it through Raycast, when available.
--
--             These functions have some weird races that make operation
--             inconsistant and difficult. But I'm leaving them in the
--             code because it documents what my descisions are.
--
--             Also the alphanumerics are 'interesting' in that they do
--             seem to map properly when switching between Dvorak and
--             Australian keyboard layouts.

  -- hs.hotkey.bind(kb_macro, 'left', LeftHalf)
  -- hs.hotkey.bind(kb_macro, 'right', RightHalf)
  -- hs.hotkey.bind(kb_macro, 'up', TopHalf)
  -- hs.hotkey.bind(kb_macro, 'down', BottomHalf)
  -- hs.hotkey.bind(kb_macro, 'return', Maximise)
  -- hs.hotkey.bind(kb_omega, 'return', AlmostMaximize)
  -- hs.hotkey.bind(kb_macro, '-', winShrink)
  -- hs.hotkey.bind(kb_macro, '=', winGrow)
  hs.hotkey.bind(kb_omega, 'up', winTaller)
  hs.hotkey.bind(kb_omega, 'down', winShorter)
  hs.hotkey.bind(kb_omega, 'right', winWider)
  hs.hotkey.bind(kb_omega, 'left', winNarrower)

  -- this arrangement makes a compass rose in Dvorak layout
  --  hs.hotkey.bind(kb_macro, 'g', winTopLeft)
  -- hs.hotkey.bind(kb_macro, 'c', winTopCenter)
  -- hs.hotkey.bind(kb_macro, 'r', winTopRight)
  -- hs.hotkey.bind(kb_macro, 'h', winLeftHalf)
  -- hs.hotkey.bind(kb_macro, 't', winCenterHalf)
  -- hs.hotkey.bind(kb_macro, 'n', winRightHalf)
  -- hs.hotkey.bind(kb_macro, 'm', winBottomLeft)
  -- hs.hotkey.bind(kb_macro, 'w', winBottomCenter)
  -- hs.hotkey.bind(kb_macro, 'v', winBottomRight)

  -- hs.hotkey.bind(kb_macro, '6', FirstTwoThirds)
  -- hs.hotkey.bind(kb_macro, '7', FirstThird)
  -- hs.hotkey.bind(kb_macro, '8', CenterThird)
  -- hs.hotkey.bind(kb_macro, '9', LastThird)
  -- hs.hotkey.bind(kb_macro, '0', LastTwoThirds)

  -- hs.hotkey.bind(kb_omega, '0', ToScreen(hs.screen'Retina'))
  -- hs.hotkey.bind(kb_omega, '7', ToScreen(hs.screen'Dell'))
  -- hs.hotkey.bind(kb_omega, '8', ToScreen(hs.screen'Phl'))
  -- hs.hotkey.bind(kb_omega, '9', ToScreen(hs.screen'SMS'))

  -- hs.hotkey.bind({'cmd', 'shift'}, ',', winPrevScreen)
  -- hs.hotkey.bind({'cmd', 'shift'}, '.', winNextScreen)


  -- Hotkey / Quake for Wezterm
  -- (I'm considering replacing iTerm2 with Wezterm since it's much
  --  faster and less battery draining)
  -- See https://github.com/wez/wezterm/issues/1751#issuecomment-1504487757

  -- hs.hotkey.bind(kb_omega, 'f12', function() hs.application.launchOrFocus('Wezterm') end)
  -- hs.hotkey.bind(kb_macro, 'f12',
  --   function()
  --     local appName = 'Wezterm'
  --     local term = hs.application.find(appName)
  --     if not term then
  --       hs.application.launchOrFocus(appName)
  --     end
  --     if term:isFrontmost() then
  --         term:hide() -- hides the whole app.
  --         -- TODO: resize or move specific window to off-screen
  --         -- local win = hs.window.focusedWindow()
  --         -- local space = hs.spaces.focusedSpace()
  --         -- hs.spaces.moveWindowToSpace(win, 1337)
  --     else
  --       term:activate()
  --       term:focus()
  --         -- local win = hs.window.focusedWindow()
  --         -- hs.spaces.moveWindowToSpace(hs.spaces.focusedSpace(), win)
  --     end
  --   end
  -- )

end
bindKeys()
