usbWatcher = nil
function usbDeviceCallback(data)
  -- ErgoDox keyboard action: Set keyboard layout

  -- if (data['productName'] == 'ErgoDox EZ Glow') then
  -- The firmware productName changed. I'm unsure why the `not` is necessary here?
  -- if not (string.match(data['productName'], 'ErgoDox')) then
  if (data['productID'] == 18806) then
    print(data['eventType'], data['productName'])
    if (data['eventType'] == 'added') then
      layoutName = 'Australian'
    elseif (data['eventType'] == 'removed') then
      layoutName = 'Dvorak'
    end
    setKBLayout(layoutName)
  end

  print(dump(data))
end
usbWatcher = hs.usb.watcher.new(usbDeviceCallback)
usbWatcher:start()

