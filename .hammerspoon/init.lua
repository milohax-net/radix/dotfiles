-- settings first
hs.window.animationDuration = 0
hs.application.enableSpotlightForNameSearches(true)

-- start the magic
require 'reload_config'
require 'util'
require 'spoons'
require 'windows'
require 'keys'
require 'usb_watcher'

-- TODO emulate Stay / automatic window layouts (single and multi-screen)
-- TODO Zoom interaction and management (there's a Zoom Spoon that could help)
