# Home directory and Dotfiles

The Unix "home directory" is where you start when you login to Unix, and
also if you run the `cd` command without any arguments. This directory's
location varies among different Unixen, and the full path for the
current user's home is stored in the Unix environment variable `$HOME`.
Most Unix utilities, such as shells, will also expand the `~`
meta-character to this directory path.

The home directory contains your personal files and sub-directories, as
well as various "dotfiles" — configuration settings for Unix utilities
that are stored in hidden files. They are hidden because by convention
the Unix `ls` command, which lists files, does not show files beggining
with a "dot" (`.`) unless directed specifically to do so, with its `-a`
(for "all") switch.

## Directories in `$HOME`

I like to keep my `$HOME` fairly neat and tidy, with well-worn pebbles
on the path. In addition to "well known directories" for application
files, such as `Desktop`, `Documents`, `Downloads`, `Pictures`, and so
on, which are desktop-dependant, I have these old ***three-letterisms***
from my early days as a Unix user. Here's a list of what they are for:

- `bak`: backups
- `bin`: my own local programs
- `etc`: a link to [XDG configuration][xdg] files in `.config`
- `fun`: games
- `hax`: hacks (throw-away projects)
- `img`: large binary dumps / disc images
- `key`: secrets and keys
- `lab`: experiments and projects
- `lib`: programming libraries and runtimes
- `man`: manual pages for programs in `bin`
- `mnt`: a place to mount filesystems temporarily (e.g. disc images)
- `net`: network drives
- `pub`: publicly shared files
- `ram`: memory drives
- `srv`: a place to serve files, such as web servers, or databases
- `tmp`: temporary files
- `var`: a link to [XDG user data][xdg] files in `.local`
- `vms`: volumes for virtual machines or containers
- `web`: publicly shared web pages

These are not always present, and they are created either by hand, or
with a workstation bootstrap script.

## Dotfiles

The hidden "dotfiles" in my `$HOME` are managed in a git repository.
This lets me make experimental changes and revert them if I don't like
them, or copy them among all my other Unix homes if I do. The focus in
this repository is purely on dotfile settings, with inspiration from
[GitHub's dotfiles collection][gh-dotfiles]. Notably absent from the
dotfiles repository are shell configurations such as for Bash, Zsh, or
Fish, which I'm keeping separately in their own repositories.

The files are all named and stored in the git repository _as expected by
each specific utility, to be installed into `$HOME`_. For example:

- `.gitconfig` in the repository is expected by `git` to be installed at
  `~/.gitconfig`
- `.config/mc` is expected by Midnight Commander to be installed at
  `~/.config/mc`

The mechanism to do this setup is _not_ a part of this repository. Use
another tool to arrange this.

### Installation with `yadm`

A good tool that does put these dotfiles into the `$HOME` directory
seamlessly, without copying or using symlinks, and while also providing
system-specific alternates and encryption, is [yadm][yadmio], which is a
wrapper around `git`.

To [install yadm][yadm-install] in a cross-platform manner (such as to
bootstrap the dotfiles onto _any_ Unix-like machine), use [this installation snippet][install-snip]:

```shell
bash -c "$(curl -fsSL https://milohax.net/dotfiles-install)"
```

(This is a shortcut to  `https://gitlab.com/milohax-net/radix/dotfiles/-/snippets/3705626/raw/main/install.sh`)

It will:

1. Install yadm from [my personal mirror in GitLab][yadm-mirror], if it's missing
1. Clone these dotfiles from GitLab into a local repository in `~/.local/share/yadm/repo.git`
2. Set the git _work-tree_ for this repository to your `$HOME`
3. Checkout the repository. If it encounters existing files with the
   same names it stops, and offers advice to continue.
5. Run `yadm checkout $HOME` to ***force an overwrite of the pre-existing
   files***.

See [the `yadm` manual][yadm-man] for full details.

---

[xdg]:          https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
[gh-dotfiles]:  https://dotfiles.github.io/bootstrap/
[yadmio]:       https://yadm.io
[yadm-man]:     https://gitlab.com/milohax-hub/yadm/-/blob/master/yadm.md
[yadm-install]: https://yadm.io/docs/install
[yadm-github]:  https://github.com/TheLocehiliosan/yadm/
[yadm-mirror]:  https://gitlab.com/milohax-hub/yadm.git
[install-snip]: https://gitlab.com/milohax-net/radix/dotfiles/-/snippets/3705626
